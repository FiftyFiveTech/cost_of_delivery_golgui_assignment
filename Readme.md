# Parcel Cost Calculator

## Description

Create a simple UI that accepts weight, height, width and length of a parcel and show the calculated Cost for the parcel using API's. 

## Implementation

Used `go-app` framework in Golang for creating PWA frontend. Here created a component `CostCalculatorGUI` that extend `app.Comp` and define user interface. `*CostCalculatorGUI.Render()` returns the UI and `*CostCalculatorGUI.formSubmit()` handles API execuation and response handling.

## Folder Structure

- `component` : DOM component
- `consts` : Constant values
- `model` : DTO, Entity & Models

## How to Run

> First start the Backend Application and Get the BASE API URL

- Clone the Git repository
- Run `go mod download` to install required packages
- Change the value of `BASE_API_URL` at `./consts/consts.go`
- Run `make run` to start local server instance
- Open `http://localhost:8082` for preview

## Package Used

- [Go-App](https://go-app.dev/)


## Dependency

- [Go v1.18+](https://go.dev/dl/)