package model

type ParcelRequest struct {
	Width  string `json:"width,omitempty"`
	Height string `json:"height,omitempty"`
	Length string `json:"length,omitempty"`
	Weight string `json:"weight,omitempty"`
}

type ParcelCostResponse struct {
	Status  int     `json:"status,omitempty"`
	Message string  `json:"message,omitempty"`
	Cost    float64 `json:"cost,omitempty"`
}
