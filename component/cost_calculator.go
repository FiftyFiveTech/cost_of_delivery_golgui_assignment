package component

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"

	"gitlab.com/FiftyFiveTech/cost_of_delivery_golang_assignment/consts"
	"gitlab.com/FiftyFiveTech/cost_of_delivery_golang_assignment/model"

	"github.com/maxence-charriere/go-app/v9/pkg/app"
)

type CostCalculatorGUI struct {
	app.Compo

	Weight string
	Height string
	Width  string
	Length string

	Message  string
	hasError bool
}

func (c *CostCalculatorGUI) Render() app.UI {
	return app.Div().Class("conatiner").Body(
		app.Div().Class("row").Body(
			app.Div().Class("column", "column-25"),

			app.Div().Class("column", "column-50").Body(
				app.H1().Text("Parcel Cost Calculator"),

				app.Form().Method("get").Body(
					app.FieldSet().Body(
						app.Label().
							Text("Weight (kg)"),
						app.Input().
							OnChange(func(ctx app.Context, _ app.Event) {
								c.Weight = ctx.JSSrc().Get("value").String()
							}).
							Required(true).
							Value(c.Weight).
							Name("weight"),

						app.Label().
							Text("Height (cm)"),
						app.Input().
							OnChange(func(ctx app.Context, _ app.Event) {
								c.Height = ctx.JSSrc().Get("value").String()
							}).
							Required(true).
							Value(c.Height).
							Name("height"),

						app.Label().
							Text("Width (cm)"),
						app.Input().
							OnChange(func(ctx app.Context, _ app.Event) {
								c.Width = ctx.JSSrc().Get("value").String()
							}).
							Required(true).
							Value(c.Width).
							Name("width"),

						app.Label().
							Text("Length (cm)"),
						app.Input().
							OnChange(func(ctx app.Context, _ app.Event) {
								c.Length = ctx.JSSrc().Get("value").String()
							}).
							Required(true).
							Value(c.Length).
							Name("length"),

						app.Button().
							Type("button").
							Class("button-primary").
							Value("Calculate Cost").
							Text("Calculate Cost").
							OnClick(c.formSubmit),

						app.Button().
							Class("button", "button-clear").
							Type("reset").
							Text("RESET").
							OnClick(c.resetValue),
					),
				),

				app.If(c.Message != "",
					app.If(c.hasError,
						app.P().
							Style("color", "red").
							Text(c.Message),
					).Else(
						app.P().
							Style("color", "green").
							Text(c.Message),
					),
				),
			),

			app.Div().Class("column column-25"),
		),
	)
}

func (c *CostCalculatorGUI) resetValue(ctx app.Context, e app.Event) {
	c.Width = ""
	c.Weight = ""
	c.Height = ""
	c.Length = ""
	c.Message = ""
	c.hasError = false
}

func (c *CostCalculatorGUI) formSubmit(ctx app.Context, e app.Event) {
	payload := model.ParcelRequest{
		Width:  c.Width,
		Height: c.Height,
		Length: c.Length,
		Weight: c.Weight,
	}

	data, _ := json.Marshal(payload)

	resp, err := http.Post(
		consts.BASE_API_URL+"/parcel/cost",
		"application/json",
		bytes.NewBuffer(data),
	)
	if err != nil {
		log.Println(err)
		c.Message = "Request Failed or Server Down"
		return
	}

	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Println(err)
		c.Message = "Not Able to Read Response"
		return
	}

	var costResponse model.ParcelCostResponse
	err = json.Unmarshal(body, &costResponse)
	if err != nil {
		log.Println(err)
		c.Message = "Not Able to Parse Response"
		return
	}

	log.Println(string(body))

	if resp.StatusCode == http.StatusBadRequest {
		c.Message = costResponse.Message
		c.hasError = true
		return
	}

	if resp.StatusCode == http.StatusOK {
		c.Message = fmt.Sprintf("Cost is: $%.2f", costResponse.Cost)
		c.hasError = false
		return
	}
}
